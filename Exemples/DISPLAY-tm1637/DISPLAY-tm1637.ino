#include <Arduino.h>
#include <TM1637Display.h>

// Module connection pins (Digital Pins)
#define CLK 2
#define DIO 3

// The amount of time (in milliseconds) between tests
#define DELAY   500


TM1637Display display(CLK, DIO);

void setup()
{
}

void loop()
{
  //display.setBrightness(0x01);
  display.setBrightness(0x0f);

  // Run through all the dots
  int hora = 3;
    
  int minuts = 03;
  int temps = hora*100+minuts;
    
  display.showNumberDecEx(temps, (0x80 >> 1), true);
  delay(DELAY);
  display.showNumberDecEx(temps, (0x80 >> 0), true);
  delay(DELAY);  
}
